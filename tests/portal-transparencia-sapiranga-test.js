module.exports = {
	"Open Site": function(browser) {
		browser
			.url(
				"https://servicos.sapiranga.rs.gov.br/ecidade_transparencia/despesas"
      )
      .execute(function() {
        js_loadMenu('1');
      })
      .waitForElementVisible('td[title="PREFEITURA MUNICIPAL DE SAPIRANGA"]', 10000)
      .click('td[title="PREFEITURA MUNICIPAL DE SAPIRANGA"]')
      .waitForElementVisible('td[title="SECRETARIA MUNICIPAL EDUCAÇÃO, CULTURA E DESPORTO"]', 10000)
      .click('td[title="SECRETARIA MUNICIPAL EDUCAÇÃO, CULTURA E DESPORTO"]')
      .pause(5000)
      .execute(function() {
        imprime('csv')
      })
      .pause(10000)
			.end();
	}
};