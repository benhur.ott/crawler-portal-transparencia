# Crawler Portal Transparência Sapiranga

## Setup

* Ter o navegador [Google Chrome](https://www.google.com/chrome/) instalado e atualizado. 
* Instalar [NodeJS 10.16+](https://nodejs.org/en/)
* Na pasta do projeto, executar `npm install`

## Executando os testes

* Rodar `npm start`

O arquivo CSV vai estar na pasta padrão de downloads.